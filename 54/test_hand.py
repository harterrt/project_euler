import unittest
import os
from Hand import Hand

class TestInit(unittest.TestCase):
    def testShortHand(self):
        """Should raise error if less than 5 cards dealt"""
        self.assertRaises(AssertionError,Hand,'2D3D4D5D')

    def testLongHand(self):
        """Should raise error if more than 5 cards dealt"""
        self.assertRaises(AssertionError,Hand,'2D3D4D5D6D7D')

    def testProperHand(self):
        self.assertEqual(Hand('2D3D4D5D6D').cards,[{'rank':0,'suit':'D'},
         {'rank':1,'suit':'D'},{'rank':2,'suit':'D'},{'rank':3,'suit':'D'},{'rank':4,'suit':'D'}])

class TestStraitFlush(unittest.TestCase):
    def testStraitFlush(self):
        self.assertEqual(Hand('2h3h4h5h6h').isStraitFlush(),(True,[4],[]))

    def testStrait(self):
        self.assertEqual(Hand('2h3h4c5h6h').isStraitFlush(),(False,[4],[]))

    def testFlush(self):
        self.assertEqual(Hand('2h3h3h5h6h').isStraitFlush(),(False,[4],[]))

    def testRoundStrait(self):
        self.assertEqual(Hand('2h3h4h5hAh').isStraitFlush(),(True,[12],[]))

class TestStrait(unittest.TestCase):
    def testStrait(self):
        self.assertEqual(Hand('2h3d4s5c6c').isStrait(),(True,[4],[]))

    def testRoundStrait(self):
        self.assertEqual(Hand('2h3d4s5cAc').isStrait(),(True,[12],[]))

    def testUnorderedStrait(self):
        self.assertEqual(Hand('KhTdQsJc9c').isStrait(),(True,[11],[]))

    def testNonStrait(self):
        self.assertEqual(Hand('Kh9dQsJc9c').isStrait(),(False,[11],[]))

class TestFlush(unittest.TestCase):
    def testFlush(self):
        self.assertEqual(Hand('2H6HKHQHTH').isFlush(),(True,[11,10,8,4,0],[]))

    def testNonFlush(self):
        self.assertEqual(Hand('2H6DKHQHTH').isFlush(),(False,[11,10,8,4,0],[]))

class TestFourOfKind(unittest.TestCase):
    def testFourOfKind(self):
        self.assertEqual(Hand('4D4H4S4C5D').isFourOfKind(),(True, [2],[]))

    def testNotFourOfKind(self):
        self.assertEqual(Hand('4D3H4S4C5D').isFourOfKind(),(False,[],[]))

class TestFullHouse(unittest.TestCase):
    def testFullHouse(self):
        self.assertEqual(Hand('3D3S4H4d3C').isFullHouse(),(True,[1],[]))

    def testNotFullHouse(self):
        self.assertEqual(Hand('3D5S4H4d3C').isFullHouse(), (False,[],[]))

class TestThreeOfKind(unittest.TestCase):
    def testThreeOfKind(self):
        self.assertEqual(Hand('3D3S2H4d3C').isThreeOfKind(),(True,[1],[2,0]))

    def testNotThreeOfKind(self):
        self.assertEqual(Hand('3D5S4H4d3C').isThreeOfKind(), (False,[],[]))

class TestTwoPair(unittest.TestCase):
    def testTwoPair(self):
        self.assertEqual(Hand('3D3S4H4d2C').isTwoPair(),(True,[2,1],[0]))

    def testNotTwoPair(self):
        self.assertEqual(Hand('3D5S4H5d2C').isTwoPair(), (False,[],[]))

class TestPair(unittest.TestCase):
    def testPair(self):
        self.assertEqual(Hand('8D3S4H4d2C').isPair(),(True,[2],[6,1,0]))

    def testNotPair(self):
        self.assertEqual(Hand('3D9S4H5d2C').isPair(), (False,[],[]))

class TestRank(unittest.TestCase):
    def testRoyalFlush(self):
        self.assertEqual(Hand('KDQDJDTDAD').rank(),(8,([12],[])))

    def testStraitFlush(self):
        self.assertEqual(Hand('KDQDJDTD9D').rank(),(8,([11],[])))

    def testFourOfKind(self):
        self.assertEqual(Hand('4D4S4C5D4H').rank(),(7,([2],[])))

    def testFullHouse(self):
        self.assertEqual(Hand('2H3D2D3H3C').rank(),(6,([1],[])))

    def testFlush(self):
        self.assertEqual(Hand('2D8D3D4D9D').rank(),(5,([7,6,2,1,0],[])))

    def testStrait(self):
        self.assertEqual(Hand('KCQDJDTDAD').rank(),(4,([12],[])))

    def testThreeOfKind(self):
        self.assertEqual(Hand('3DTCKC3H3C').rank(),(3,([1],[11,8])))

    def testTwoPair(self):
        self.assertEqual(Hand('2C2D3C4D3H').rank(),(2,([1,0],[2])))

    def testPair(self):
        self.assertEqual(Hand('2C3D4H5H2H').rank(),(1,([0],[3,2,1])))

    def testHighCard(self):
        self.assertEqual(Hand('AC5DTH2C3D').rank(),(0,([12,8,3,1,0],[])))



if __name__ == "__main__":
    unittest.main()
