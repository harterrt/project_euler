class Hand:

    def __init__(self, strHand):
        rankEnum = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
        rankDict = dict(zip(rankEnum,range(len(rankEnum))))

        self.cards = []
        assert(len(strHand)==10)

        for index in range(0,10,2):
            self.cards.append(dict())
            self.cards[index/2]['rank'] = rankDict[strHand[index]]
            self.cards[index/2]['suit'] = strHand[index+1]
            # print index, strHand[index], strHand[index+1], self.cards[index/2], self.cards

    def rank(self):

        isRank = [
                self.isStraitFlush(),
                self.isFourOfKind(),
                self.isFullHouse(),
                self.isFlush(),
                self.isStrait(),
                self.isThreeOfKind(),
                self.isTwoPair(),
                self.isPair()
                 ]
        
        for index in range(len(isRank)):
            if isRank[index][0]:
                return len(isRank)-index, isRank[index][1:]

        return 0, (sorted([card['rank'] for card in self.cards], reverse=True), [])

    def isStraitFlush(self):
        return self.isFlush()[0] and self.isStrait()[0], self.isStrait()[1], []

    def isFlush(self):
        match = True
        for card in self.cards:
            match = match and card['suit'] == self.cards[0]['suit']

        return match, sorted([card['rank'] for card in self.cards],reverse=True), []

    def isStrait(self):
        sortRank = sorted([card['rank'] for card in self.cards])

        match = True
        for ind in range(1,5):
            match = match and sortRank[ind-1] == sortRank[ind]-1

        high = sortRank[4]

        # Account for A2345 Strait
        if sortRank == [0,1,2,3,12]:
            match = True
            high = 12

        return match, [high], []

    def isFourOfKind(self):
        ranks = [card['rank'] for card in self.cards]

        if ranks.count(ranks[0])==4:
            return True, [ranks[0]], []
        elif ranks.count(ranks[1])==4:
            return True, [ranks[1]], []
        else:
            return False, [], []

    def isFullHouse(self):
        ranks = [card['rank'] for card in self.cards]
        ranksCount = map(ranks.count, ranks)

        if ranksCount.count(3)==3 and ranksCount.count(2)==2:
            return True, [ranks[ranksCount.index(3)]], []
        else:
            return False, [], []
            
# All Methods following this need test cases.

    def isThreeOfKind(self):
        ranks = [card['rank'] for card in self.cards]
        ranksCount = map(ranks.count, ranks)

        if ranksCount.count(3)==3:
            tripleRank = ranks[ranksCount.index(3)]

            highCards = [rank for rank in ranks if rank!=tripleRank]
            highCards.sort(reverse=True)

            return True, [tripleRank], highCards
        else:
            return False, [], []
            
    def isPair(self):
        ranks = [card['rank'] for card in self.cards]
        ranksCount = map(ranks.count, ranks)

        if ranksCount.count(2)==2:
            pairRank = ranks[ranksCount.index(2)]

            highCards = [rank for rank in ranks if rank!=pairRank]
            highCards.sort(reverse=True)

            return True, [pairRank], highCards
        else:
            return False,[],[]
            
    def isTwoPair(self):
        ranks = [card['rank'] for card in self.cards]
        ranksCount = map(ranks.count, ranks)

        if ranksCount.count(2)==4:
            pairRank = [rank for rank in ranks if ranks.count(rank)==2]
            #Remove Duplicates
            pairRank = list(set(pairRank))
            pairRank.sort(reverse=True)

            highCard = ranks[ranksCount.index(1)]

            return True, pairRank, [highCard]
        else:
            return False,[],[]
            
