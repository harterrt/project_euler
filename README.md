Introduction
============
"[ProjectEuler](http://www.projecteuler.net) is a series of challenging mathematical/computer programming problems that will require more
than just mathematical insight to solve"

This is a collection of tools I've built over the course of several years to solve a subset of the ProjectEuler problems. I started
this project as a way to test myself as I self-learned Java. Since then this project has served as a testing ground for c, c++, and python.

Sadly, I didn't know about source control when I started this project and my documentation standards were weak at best. I do plan on cleaning
this code for posterity's sake. I submit problems 16-53 to the public eye, as is and without regrets; I learned a lot from that shoe-string code.
I will try to produce better documentation and more useful source control with future problems.
