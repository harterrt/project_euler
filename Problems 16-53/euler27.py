import EulerModule

def consPrimes(aa, bb):
	isPrime = True
	index = 0
	while isPrime:
		test = (index * index) + (aa * index) + bb
		isPrime = EulerModule.isPrime( test )
		index = index + 1

	return index - 1

def answer():
	biggest = (0, 0, 0)
	for aa in range(-999, 1000):
		if aa % 100 == 0:
			print aa
			print biggest
		for bb in range(0 , 1000):
			length = consPrimes(aa, bb)
			if length >= biggest[2]:
				biggest = (aa, bb, length)
	return biggest
