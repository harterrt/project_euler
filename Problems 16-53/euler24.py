import math

# Returns the N+1 lexiographic sequence of a set of ordered numbers
def nthLexiPerm(n, array):
	if fact(len(array)) < n:
		print("No go, Champ...")
		exit()
	if (len(array) == 1) and (n <= 1):
		return array
	else:
		index = int(math.floor(n/fact(len(array)-1)))
		number = array[index]
		array.remove(number)
		output = [number] + nthLexiPerm(n - (index * fact(len(array))), array)
		return output

def fact(x):
	if x == 1:
		return 1
	else:
		return x*fact(x-1)

print(nthLexiPerm(999999, list(range(10))))
