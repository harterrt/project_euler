from math import sqrt
import EulerModule

def main():
	found = False
	index = 0
	numbers = []
	while not found:
		index = index + 1
		test = index * ((3 * index) - 1) / 2
		if index % 100 == 0:
			print index
		
		for pentagonal in numbers:
			if EulerModule.searchList(numbers, test - pentagonal) != -1:
				sum = pentagonal + test
				root = (1 + sqrt( 1 + (24 * sum) ))/6
				if int( root ) == root:
					found = True
					output = (test, pentagonal, test - pentagonal)
		numbers.append(test)

	return output
