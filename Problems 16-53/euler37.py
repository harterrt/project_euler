import EulerModule

def main():
	output = 0
	primes = EulerModule.seive(1000000)
	count = 0
	index = 0

	while (count < 16) and (index < len(primes)):
		isTruncable = True
		for trunc in truncations( primes[index] ):
			if EulerModule.searchList( primes, trunc) == -1:
				isTruncable = False
		if isTruncable:
			output = output + primes[index]
			count = count + 1
			print primes[index]
		index = index + 1
	
	return output, count

def truncations( number ):
	output = set()

	length = 0
	temp = number / 10
	while temp > 0:
		output.add(temp)
		temp = temp / 10
		length = length + 1
	
	temp = number
	for index in range(1, length + 1):
		output.add( temp % (10 ** index))
	
	return output
