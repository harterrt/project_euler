import EulerModule

def oddFraction( aa, bb, cc):
	output = []

	#Check form AB/AC
	num = (aa * 10) + bb
	den = (aa * 10) + cc
	if num/den < 1:
		if equalFractions(num, den, bb, cc):
			output = output + [(num, den)]

	# Check form AB/CA
	num = (aa * 10) + bb
	den = (cc * 10) + aa
	if num/den < 1:
		if equalFractions(num, den, bb, cc):
			output = output + [(num, den)]

	# Check form BA/CA
	num = (bb * 10) + aa
	den = (cc * 10) + aa
	if num/den < 1:
		if equalFractions(num, den, bb, cc):
			output = output + [(num, den)]

	# Check form BA/AC
	num = (bb * 10) + aa
	den = (aa * 10) + cc
	if num/den < 1:
		if equalFractions(num, den, bb, cc):
			output = output + [(num, den)]
	
	return output

def equalFractions(num1, den1, num2, den2):

	return reduce(num1, den1) == reduce(num2, den2)
	
def reduce(num, den):
	factNum = EulerModule.factor(num)
	factDen = EulerModule.factor(den)

	for index in factNum:
		while (index in factDen) and (index in factNum):
			factDen.remove(index)
			factNum.remove(index)
	num = 1
	den = 1

	for index in factNum:
		num = num * index

	for index in factDen:
		den = den * index
	
	return num, den
