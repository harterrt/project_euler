public class euler19
{
	public static void main(String args[])
	{
		final int [] REGULAR_YEAR = {31,28,31,30,31,30,31,31,30,31,30,31};
		final int [] LEAP_YEAR = {31,29,31,30,31,30,31,31,30,31,30,31};
		int year;
		int total;
		int janFirst;
		total = 0;

		janFirst = 3;

		for (year = 1901; year <= 2000; year++)
		{
			if (year%4 == 0)
			{
				total = total + getFirstSundays(janFirst, LEAP_YEAR);
				janFirst = (janFirst + 366) % 7;
			}
			else
			{
				total = total + getFirstSundays(janFirst, REGULAR_YEAR);
				janFirst = (janFirst + 365) % 7;
			}
		}

		System.out.println(total);

//		System.out.println(getFirstSundays(5, REGULAR_YEAR));
	}
	private static int getFirstSundays(int janFirst, int [] monDayCount)
	{
		int total;
		int month;
		int firstDay;

		firstDay = janFirst;
		total = 0;

		for (month = 0; month < 12; month++)
		{
			if (firstDay == 1)
			{
				total++;
			}
			firstDay = firstDay + monDayCount[month];
			firstDay = firstDay % 7;
		}
		return total;
	}
}
