from math import sqrt


def sumDiag( size ):
	'''Only programmed to work for odd spirals (5x5, 7x7)'''
	step = 1
	sum = 1
	for increment in range (1, ( size+1 )/2):
		for index in range (1, 5):
			step = step + increment * 2
			sum = sum + step
			print sum

	return sum
