from math import sqrt

def convertFile():
	output = []

	NAME = "words.txt"
	FILE = open(NAME, 'r')
	line = FILE.next()
	words = line.split("\",\"")
	
	convert = dict(zip('\nabcdefghijklmnopqrstuvwxyz', range(0,27)))

	for word in words:
		print word
		sum = 0
		for letter in word.lower():
			sum = sum + convert[letter]
		output.append(sum)
	
	# Here's the fun part. n*(n+1)/2 = number so n**2 + n - 2*number = 0
	#  these solutions are (-1+-sqrt(1+8*number))/2. b/c number > 0, 
	#  it is sufficient to say that if sqrt(1+8*number) is int we have a 
	#  triangle number
	sum = 0
	for number in output:
		test = sqrt(1 + (8 * number))
		if test == int(test):
			sum = sum + 1

	# First and last enteries in output are false 0's
	return sum - 2

		
