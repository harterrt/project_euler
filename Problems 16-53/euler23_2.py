import EulerModule

def abundantArray(number):
	output = []
	for index in range(1,number):
		if EulerModule.checkPerfection(index) == 1:
			output.append(index)
#		if index % 1000 == 0:
#			print(index,1)
	return output

def isSumAbundant(number, array):
	output = False
	index = 0
	while ((not output) and (array[index] <= number/2)):
#		if EulerModule.checkPerfection(number - array[index]) == 1:
		index2 = index
		while (not output) and (array[index2] < number):
			if array[index2] == number - array[index]:
				output = True
			index2 = index2 + 1
		index = index + 1
	return output

# Make an array of all necessary abundant numbers
abundArray = abundantArray(31000)

# Make a set of all sums of abundant numbers < 30,000
#	set used because there were multiple copies of some numbers
total = set([])
for row in range (0,len(abundArray)):
	if (row%100 == 0):
		print(row)
	for col in range(row, len(abundArray)):
		if abundArray[row] + abundArray[col] <= 30000:
			total.add(abundArray[row] + abundArray[col])

# Subtract off each sum of abundant numbers from sum of all numbers less than 30,000 (n)*(n+1)/2
fuck = 0
for index in total:
	fuck = fuck + index

notAbundSum = 15000*(30001) - fuck
print(notAbundSum)
