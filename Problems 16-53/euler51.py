import EulerModule

def main():
	MAX = 1000000

	primes = EulerModule.primeList(MAX)
	for index in range(0, len(primes)):
		prime = primes[index]
		for starSet in relatedNumbers(prime):
			count = 0
			for number in starSet:
				if EulerModule.searchList(primes, number) != -1:
					count += 1
			if count >= 8:
				return prime
				

def relatedNumbers(number):
	output = []

	for indexies in possibleStars( number ):
		starSet = []
		if EulerModule.searchList(indexies, 0) != -1:
			begin = 1
		else:
			begin = 0
		for digit in range(begin, 10):
			temp = number
			for index in range(len(indexies) - 1, -1, -1):
				temp = replaceDigit(indexies[index], temp, digit)
			starSet.append(temp)
		output.append(starSet)
	
	return output


def possibleStars( number ):
	output = []

	for repDig in repeatedDigits( number ):
		output += starIndexies( number, repDig )
	
	for index in range(0, len(str(number))):
		output.append([index])
	
	return output


def starIndexies( number, repDigit ):
	output = []

	patterns = permuteBinary( len(repDigit[1]) )
	patterns.remove( '1'*len(repDigit[1]) )

	for pattern in patterns:
		indexies = repDigit[1]
		include = []
		for index in range(0, len(indexies)):
			if pattern[index] != '1':
				include.append( indexies[index] )
		if len(include) != 1:
			output.append( include )
	
	return output

def replaceDigit( index, number, digit ):
	temp = str(number)

	return int(temp[0:index] + str(digit) + temp[index + 1:len(temp)])
			
def repeatedDigits( number ):
	output = []
	count = []

	#Populate count with 0's
	for index in range(0,10):
		count.append([])

	#Append index of each occurance of a digit
	numString = str( number )
	for index in range( 0, len(numString) ):
		digit = int( numString[ index ] )
		count[digit].append( index )
	
	for index in range(0, 10):
		if len(count[index]) > 1:
			output.append( (index, count[index]) )
	
	return output

def permuteBinary( length ):
	output = []

	if length == 1:
		output = ['1','0']
	else:
		for permute in permuteBinary( length - 1 ):
			output.append('1' + permute)
			output.append('0' + permute)
	return output
