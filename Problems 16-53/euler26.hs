primes = [x | x <-2:[3,5..], (length (divisors x)) == 2]

isPrime :: (Integral a)=> a -> Bool
isPrime x = if x <= 1 then False
			else length (divisors x) == 2

divisors :: (Integral a)=> a -> [a]
divisors x = if x <= 1 then []
			 else [i | i <- [1..x], (x `mod` i) == 1]

baseFunction :: (Num b) => b -> b -> [Integral]
baseFunction a, b, i = [x*x + a*x + b | x <- [1..i]]
