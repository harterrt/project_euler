
def fib(number):
	aa, bb = 1, 1
	index = 2
	while bb <= number:
		temp = aa + bb
		aa = bb
		bb = temp
		index = index + 1
	return index

print(fib(10**999))
