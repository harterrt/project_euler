import EulerModule

def nextPrime( primes ):
	test = primes[len(primes) - 1]
	found = False

	while not found:
		test = test + 2
		isPrime = True
		for prime in primes:
			if test % prime == 0:
				isPrime = False
		if isPrime:
			found = True
	
	return test

def main():
	found = False
	test = 7
	primes = [2,3]

	while not found:
		test = test + 2
		index = 0
		doubSquare = 2

		while primes[len(primes) - 1] <= test:
			primes.append(nextPrime(primes))

		special = False
		while doubSquare <= test:
			doubSquare = index * index * 2
			if EulerModule.searchList(primes, test - doubSquare) != -1:
				special = True
			index = index + 1

		if not special:
			return test

