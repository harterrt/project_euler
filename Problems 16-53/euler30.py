
def sumOfFifthPoweredDigits( number ):
	sigDigLeft = True
	index = 1
	sum = 0
	while number > 0:
		digit = number % (10 ** index)
		number = number / (10 ** index)
		sum = sum + (digit ** 5)
	return sum

def sumOfFourthPoweredDigits( number ):
	sigDigLeft = True
	index = 1
	sum = 0
	while number > 0:
		digit = number % (10 ** index)
		number = number / (10 ** index)
		sum = sum + (digit ** 4)
	return sum

def answer():
	sum = 0
	for index in range (2, 999999):
		if index == sumOfFifthPoweredDigits( index ):
			sum = sum + index
	return sum

