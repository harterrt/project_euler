import EulerModule

def main():
'''This is shitty code. Should have used a primes sieve concept...'''
	aa = 1
	factors = []
	primes = [2,3]

	count = 0
	while count < 4:
		index = aa + count
		if fourDistinctFactors(factors, index, primes):
			count = count + 1
		else:
			aa = aa + 1
			count = 0
			factors = []
			if aa %1000 == 0:
				print aa
		if count > 2:
			print count, aa
	
	return aa



def fourDistinctFactors(factors, number, primes):

	numberFact = EulerModule.factor(number, primes)
	fourDistinct = True

	for factor in numberFact: 
		if EulerModule.searchList( factors, factor ) == -1:
			factors.append(factor)
		else:
			fourDistinct = False
	
	return (fourDistinct and (len(numberFact) == 4))
		
