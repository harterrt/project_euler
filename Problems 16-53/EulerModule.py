from math import sqrt

def factor(number, primes=[2,3] ):
	output = []

	while primes[len(primes)-1] < number:
		primes.append(nextPrime(primes))

	if number > 1:
		for index in primes:
			power = 0
			while number % index == 0:
				power = power + 1
				number = number / index
			if power > 0:
				output.append((index, power))
	elif number == 1:
		output = [(1, 1)]
	else:
		raise Exception("Cannot factor number <= 1")
	return output

def nextPrime( primes ):
	test = primes[len(primes) - 1]
	found = False

	while not found:
		test = test + 2
		isPrime = True
		for prime in primes:
			if test % prime == 0:
				isPrime = False
		if isPrime:
			found = True
	
	return test


def searchList(field, target):
	begin = 0
	end = len(field) - 1
	test = 0
	while begin < end:
		#print begin, end
		test = (begin + end)/2
		if field[test] < target:
			begin = test + 1
		else :
			end = test

	if len(field) == 0:
		output = -1
	elif target == field[begin]:
		output = begin
	else:
		output = -1
	
	return output
		

def primeList(max):
	aa = [False]
	
	# Expand array to max lenth
	for index in range(0,max - 1):
		aa.append(True)
	
	count = 0
	for index in aa:
		count = count + 1
		if index:
			for multiple in range(2, max / count + 1):
				aa[multiple * count -1] = False
	
	output = []
	count = 0
	for index in aa:
		count = count + 1
		if index:
			output.append(count)
	
	return output

def isPrime(number):
	output = True
	if number > 2:
		for index in primeList( int(sqrt(number)) ):
			if number % index == 0:
				output = False
		return output
	elif number == 2:
		return True
	else:
		return False

def properFactors(number):
	output = set()
	if number > 0:
		for index in range(1, int(sqrt(number)+1)):
			if number % index == 0:
				output.add(index)
				output.add(number/index)
		output.remove(number)
	else:
		output = set()
	return list(output)

def checkPerfection(number):
	array = properFactors(number)
	total = 0;
	for index in array:
		total =total + index
	
	if total > number:
		output = 1
	elif total == number:
		output = 0
	elif total < number:
		output = -1
	return output

def isPandigital( number ):
	'''Checks if a number contains all digits [1-9] exactly once'''
	output = True

	numberList = list(str(number))
	numberList.sort()
	for digit in range (1,10):
		#Digit found at least once
		if searchList(numberList, str(digit)) == -1:
			output = False
		else:
			# Digit found Exactly once
			numberList.remove(str(digit))
			if searchList(numberList, str(digit)) != -1:
				output = False
	
	return output
	
def permuteString( elements ):
	output = []

	if len(elements) == 1:
		output.append(elements)
	else:
		for item in elements:
			temp = elements
			temp = temp.replace(item, '')
			for permutation in permuteString(temp):
				output.append(item + permutation)
	
	return output

def permuteList( sequence=[] ):
	output = []

	if len(sequence) == 1:
		output = sequence
	else:
		for index in sequence:
			print index
			temp = sequence
			temp.remove(index)
			output.append(permuteList(temp).append(index))

	return output

def permuteString( string ):
	output = []
	if len(string) == 1:
		output = [string]
	else:
		for letter in string:
			temp = string
			temp = temp[0:temp.index(letter)] + temp[(temp.index(letter) + 1):(len(temp) + 1)]
			for permute in permuteString( temp ):
				output.append( letter + permute )
	return output
