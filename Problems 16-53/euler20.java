public class euler20
{
	public static void main(String args[])
	{
		node currentNode;
		currentNode = new node();
		int index;
		int total;
		total = 0;

		currentNode = linkedListFactorial(100);

//		for (index = 1; index <=10; index++)
//		{
//			currentNode = multiplyLinkedList(2,currentNode);
//			System.out.println(index);
//		}
//		currentNode = multiplyLinkedList(32, currentNode);
//		currentNode = multiplyLinkedList(2, currentNode);
//		currentNode = multiplyLinkedList(2, currentNode);
//		currentNode = multiplyLinkedList(3, currentNode);
//		currentNode = multiplyLinkedList(4, currentNode);
//		currentNode = multiplyLinkedList(1, currentNode);
//
//		currentNode = linkedListFactorial(10);
//
//		currentNode = multiplyLinkedList(100, currentNode);
//		appendDigits(currentNode, 100);
//
		while(currentNode != null)
		{
			total += currentNode.data;
//			System.out.printf("%d",currentNode.data);
			currentNode = currentNode.link;
		}
		System.out.println(total);
	}
	private static class node
	{
		int data;
		node link;

		public node()
		{
			data = 1;
			link = null;
		}

	}
	private static node multiplyLinkedList(int mult, node linkedList)
	{
		node currentNode;
		int carryOver;
		int ones;
		currentNode = linkedList;
		carryOver = 0;
		int index;
		index = 0;

		while (currentNode != null)
		{
			index++;
			currentNode.data = currentNode.data * mult;
			currentNode.data += carryOver;

			if (currentNode.data >= 10)
			{
				carryOver = currentNode.data / 10;
				currentNode.data = currentNode.data % 10;
			}
			else
			{
				carryOver = 0;
			}
			if (currentNode.link == null)
			{
				if(carryOver > 0)
				{
					currentNode = appendDigits(currentNode, carryOver);
				}
				else
				{
					currentNode = currentNode.link;
				}
			}
			else
			{
				currentNode = currentNode.link;
			}
		}
		return linkedList;
	}

	private static node appendDigits(node currentNode, int carryOver)
	{
		while (carryOver > 0)
		{
			currentNode.link = new node();
			currentNode = currentNode.link;
			currentNode.data = carryOver % 10;
			carryOver = carryOver / 10;
		}
		return currentNode.link;
	}
	private static node linkedListFactorial(int factorial)
	{
		node linkedList;
		node currentNode;
		int index;

		linkedList = new node();
		currentNode = linkedList;

		for (index = 2; index <= factorial; index++)
		{
			linkedList = multiplyLinkedList(index, currentNode);
			currentNode = linkedList;
//			System.out.println(index);
		}
		return linkedList;
	}
}
