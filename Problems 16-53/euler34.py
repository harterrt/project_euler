
def fact(n):
	output = 1
	for index in range (n, 0, -1):
		output = output * index
	return output

def factFast(n):
	temp = [1,1,2,6,24,120,720,5040,40320,362880]
	return temp[n]

def answer():
	sum = 0
	for index in range(3, 7*fact(9)):
		if index % 100000 == 0:
			print index
		if sumOfDigFact(index):
			sum = sum + index
			print index
	return sum

def sumOfDigFact( number ):
	sum = 0
	temp = number

	while temp > 0:
		digit = temp % 10
		temp = temp / 10
		sum = sum + factFast(digit)

	return sum == number 
