import EulerModule

def main():
	largest = 0

	for base in range (1, 10000):
		digits = 0
		index = 1
		conProd = ""
		while digits < 9:
			part = str(base * index)
			digits = digits + len( part )
			conProd = conProd + part
			index = index + 1

		intProd = int(conProd)
		if digits == 9:
			if intProd > largest:
				if EulerModule.isPandigital( intProd ):
					largest = intProd
	
	return largest
