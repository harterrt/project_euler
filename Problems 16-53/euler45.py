from math import sqrt

def main():
	index = 143
	found = False

	while not found:
		index = index + 1
		hex = hexagonal(index)
		if index % 10000 == 0:
			print index, hex

		if isPent(hex) and isTri(hex):
			found = True

	return hex
	
def isTri( number ):
	root = (sqrt( 1 + (8 * number) ) - 1)/2

	return int( root ) == root

def hexagonal( index ):
	return index * ((index * 2) - 1)

def isPent( number ):
	root = (1 + sqrt( 1 + (24 * number) ))/6

	return int(root) == root

