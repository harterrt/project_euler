import EulerModule

def tenDigPandigital():
	output = []

	for number in EulerModule.permuteString('123456789'):
		for index in range(1, 10):
			output.append( number[0:index] + '0' + number[index:9])
	
	return output

def main():
	output = 0

	primes = [2, 3, 5, 7, 11, 13, 17]
	for pandigital in tenDigPandigital():
		curious = True
		for index in range(1, 8):
			number = int( pandigital[ index:(index + 3) ] )
			if number % primes[index - 1] != 0:
				curious = False
		if curious:
			output = output + int(pandigital)
	
	return output
