import EulerModule

def rotations( prime ):
	output = [prime]
	
	temp = prime
	length = 0
	while temp > 0:
		temp = temp / 10
		length = length + 1
	
	rotation = prime
	for count in range(1, length):
		temp = rotation % 10
		temp = temp * (10 ** (length - 1))
		rotation = rotation / 10
		rotation = rotation + temp

		output.append(rotation)
	
	return output

def main():
	primes = EulerModule.seive(1000000)

	count = 0
	circularPrimes = 0
	for index in primes:
		print index

		test = rotations(index)
		circular = True
		for rot in test:
			isPrime = False

			count = 0
			while (count < len(primes)) and (primes[count] <= rot):
				if primes[count] == rot:
					isPrime = True
				count = count + 1

			if not isPrime:
				circular = False
		if circular:
			circularPrimes = circularPrimes + 1

	return circularPrimes
				
				

