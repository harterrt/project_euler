def nextDigit(num, den):
	output = 0
	rem = 0
	while output == 0 and num != 0:
		output = num / den
		rem = num % den
		num = num * 10
	return output, rem

def repLength(aa):
	prevNum = []
	series = []
	num, den = 1, aa
	while not num in prevNum:
		prevNum.append(num)
		digit, num = nextDigit(num, den)
		series.append(digit)
	return len(series) - prevNum.index(num)

print(repLength(6))

longest = 0
for index in range(1,1000):
	length = repLength(index)
	if length > longest:
		longest = length
		number = index
print(number)
