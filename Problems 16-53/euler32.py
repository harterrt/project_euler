
def listOfDig(number):
	digitsLeft = True
	index = 1
	output = []
	while number > 0:
		digit = number % (10**index)
		number = number / 10
		output.append(digit)
	return output

def uniqueDigits(number):
	testSet = set()
	testList = listOfDig(number)
	list.sort(testList)
	for index in testList:
#		if index == 0:
#			return False
		testSet.add(index)
	temp = list(testSet)
	list.sort(temp)
	if temp == testList:
		return True
	else:
		return False

def answer():
	temp = set()
	for yy in range(1234, 9877):
		if uniqueDigits(yy):
			for aa in range(2, 100):
				if uniqueDigits(aa*10000 + yy):
					tempList = []
					if yy % aa == 0:
						tempList = tempList + listOfDig(aa*10000 + yy) + listOfDig(yy/aa)
						list.sort(tempList)
						if tempList == [1,2,3,4,5,6,7,8,9]:
							temp.add(yy)
	sum = 0
	for index in temp:
		sum = sum + index
	return sum
