import EulerModule

def rotations( prime ):
	output = [prime]
	
	temp = prime
	length = 0
	while temp > 0:
		temp = temp / 10
		length = length + 1
	
	rotation = prime
	for count in range(1, length):
		temp = rotation % 10
		temp = temp * (10 ** (length - 1))
		rotation = rotation / 10
		rotation = rotation + temp

		output.append(rotation)
	
	return output

def main():
	primes = EulerModule.seive(1000000)

	circularPrimes = 0
	for index in primes:
		#print index

		test = rotations(index)
		circular = True

		for rot in test:
			if EulerModule.searchList(primes, rot) == -1:
				circular = False

		if circular:
			circularPrimes = circularPrimes + 1

	return circularPrimes
				
				

