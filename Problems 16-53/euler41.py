import EulerModule
from math import sqrt

def main():
	primes = EulerModule.primeList(32622)
	panDigitals = panDigList()

	for index in range(len(panDigitals) - 1, 0, -1):
		test = int(panDigitals[index])
		if index %1000 == 0:
			print test
		isPrime = True
		for prime in primes:
			if prime < sqrt(test):
				if test % prime == 0:
					isPrime = False
		if isPrime:
			return test


def panDigList():
	output = []
	# note: 8 and 9 digit pandigitals are divisible by 3
	for index in range(1, 8):
		digits = range(1, index + 1)
		digits = toString( digits )
		output = output + permuteString( digits )
	
	return output

def permuteString( elements ):
	output = []

	if len(elements) == 1:
		output.append(elements)
	else:
		for item in elements:
			temp = elements
			temp = temp.replace(item, '')
			for permutation in permuteString(temp):
				output.append(item + permutation)
	
	return output

def toString( input ):
	output = ""

	for index in input:
		output = output + str(index)
	
	return output
