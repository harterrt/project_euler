def main():
	output = 0

	for index in binaryPalinLessThan(1000000):
		decimal = convertToDecimal(index)
		if isPalin(decimal):
			output = output + int(decimal)
	
	return output

def convertToDecimal( binary ):
	count = 0
	decimal = 0
	temp = reverseString(binary)
	for number in temp:
		decimal = decimal + (int(number) * (2 ** count))
		count = count + 1
	
	return str(decimal)

def binaryPalin(length):
	'''Returns all binary palindrodes of a certain length'''
	output = []

	tails = binaryPerm( length / 2 )
	for tail in tails:
		if length % 2 == 0:
			#Combine head and tail
			head = reverseString(tail)
			output.append(head + tail)

		else:
			#Spit head and tail with a 1
			head = reverseString(tail)
			output.append(head + "1" + tail)
			
			#Spit head and tail with a 0
			head = reverseString(tail)
			output.append(head + "0" + tail)

	return output

def binaryPalinLessThan( max ):
	output = []
	maxReached = False
	count = 2
	max = int(decimalToBinary(max))
	
	while not maxReached:
		for palin in binaryPalin(count):
			if int(palin) >= max:
				maxReached = True
			else:
				output.append(palin)
		count = count + 1
	output.insert(0, "1")

	return output

def reverseString( input ):
	output = ""

	for letter in input:
		output = letter + output
	
	return output

def decimalToBinary(decimal):
	count = 0
	output = ""
	
	if decimal < 0:
		raise Exception("Input to decimalToBinary must be non-negative")
	elif decimal == 0:
		output = 0

	while decimal > 0:
		output = str(decimal % 2) + output
		decimal = decimal >> 1
	
	return output

def isPalin( string ):
	'''Test if string is palindrone'''
	reverse = reverseString( string )

	return string == reverse

def binaryPerm(length):
	'''Returns all possible permutations of 1 & 0 of a certain length, 
		to be used in Project Euler 36, recursively. This means that 
		there must be a 1 in the last position. This is used to 
		create the left hand tail of the binary palindrone.'''
	output = []

	if length == 1:
		output = ["1"]
	else:
		for index in binaryPerm(length - 1):
			output.append("1" + index)
			output.append("0" + index)
	
	return output
