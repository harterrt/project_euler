
def main():
	index = 1
	string = ""
	while len(string) < 1000000:
		string = string + str(index)
		index = index + 1
	
	product = 1
	for index in range(0, 7):
		product = product * int(string[(10 ** index) - 1])
	
	return product
