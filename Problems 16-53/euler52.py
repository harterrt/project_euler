

def natNumberToList( number ):
	output = []

	while number > 0:
		output.insert(0, number % 10)
		number = number / 10
	
	return output

def sameDigits( aa, bb ):
	aList = natNumberToList(aa)
	bList = natNumberToList(bb)

	aList.sort()
	bList.sort()

	return aList == bList

def special( number ):
	output = True

	for index in range(2, 7):
		if not sameDigits( number, number * index):
			output = False
	
	return output

def main():
	
	found = False
	test = 1
	while not found:
		if test %1000 == 0:
			print test
		if special(test):
			output = test
			found = True
		test = test + 1
	
	return output
