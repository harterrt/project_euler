import EulerModule

def abundantArray(number):
	output = []
	for index in range(1,number):
		if EulerModule.checkPerfection(index) == 1:
			output.append(index)
		if index % 1000 == 0:
			print(index,1)
	return output

def isSumAbundant(number, array):
	output = False
	index = 0
	while ((not output) and (array[index] <= number/2)):
#		if EulerModule.checkPerfection(number - array[index]) == 1:
		index2 = index
		while (not output) and (array[index2] < number):
			if array[index2] == number - array[index]:
				output = True
			index2 = index2 + 1
		index = index + 1
	return output

abundArray = abundantArray(28124)
print(len(abundArray))
for candidate in range(1, 28124):
	array = []
	if not isSumAbundant(candidate, abundArray):
		array.append(candidate)
	if (candidate % 100) == 0:
		print(candidate)

total = 0
for index in array:
	total += index

print(total)
