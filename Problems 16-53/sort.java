public class sort
{
	static int count = 1;
	public static String[] quickSortStrings(String [] array, int left, int right)
	{
		int pivotIndex;
		pivotIndex = (left + right)/2;
		System.out.printf("quickSort Called w/ pivotIndex = %d, left = %d right = %d\n", pivotIndex, left, right);
		if (right > left)
		{
			pivotIndex = partition(array, left, right, pivotIndex);
			quickSortStrings(array, left, pivotIndex - 1);
			quickSortStrings(array, pivotIndex + 1, right);
		}
		return array;
	}

	private static int partition(String [] array, int left, int right, int pivotIndex)
	{
		String pivotValue;
		int storeIndex;
		int index;
		
		System.out.printf("partition called w left = %d, right = %d and pivotIndex = %d\n", left,right,pivotIndex);
		pivotValue = array[pivotIndex];
		storeIndex = left;
		for (index = left; index <= right; index++)
		{
			if (array[index].compareTo(pivotValue) <= 0)
			{
				System.out.printf("%s moved to place %d because it is less than %s\n",array[index],storeIndex,pivotValue);
				swap(array, index, storeIndex);
				storeIndex++;
			}
		}
		System.out.printf("partition returned %d\n",storeIndex);
		return storeIndex -1;
	}

	private static void swap(String [] array, int index1, int index2)
	{
		String temp;

		temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}

	public static void main( String [] args)
	{
		int index;
		String [] array = {"Fire","Tire","Wire","Aaa","Bbb","Ccc","Emily"};
		quickSortStrings(array, 0, array.length - 1);
//		java.util.Arrays.sort(array);
		
		for(index = 0; index < array.length; index++)
		{
			System.out.println(array[index]);
		}
	}
}
