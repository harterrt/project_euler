import EulerModule

def main():
	output = 0
	temp = 0

	for nn in range (1, 101):
		for rr in range(1, nn):
			if nCr(nn, rr) > 1000000:
				output += 1
	return output

def nCr(nn, rr):
	return fact(nn)/(fact(nn - rr) * fact(rr))

def fact(number):
	if number > 0:
		return number * fact(number - 1)
	else:
		return 1
