#include <stdio.h>
#include <stdlib.h>

typedef struct
	{
	int key;
	int digit;
	}DATA;

typedef struct nodeTag
	{
	DATA data;
	struct nodeTag *link;
	}NODE;

void newPower (NODE *pPre, int carryOver);
NODE *powerTwo(int power);


int main(void){
	NODE *start;
	NODE *temp;
	int sum=0;
//	NODE abc;

	start=powerTwo(1000);
//	start=&abc;
//	start->data.digit=12;

//	printf("%p\n", start->link);
//	temp= start->link;
//	printf("%p\n",temp);
//	start = temp;
//	printf("%p\n", start);

	while (!(start==NULL))
		{
		sum=sum+start->data.digit;
		start=start->link;
		}
	printf("%d",sum);
	return 0;
	}

void newPower (NODE *pPre,int value)
	{
	NODE *pNew;
	pNew = malloc(sizeof(NODE));
	
	 if(pNew==NULL)
		{exit(100);}

	pNew->data.digit=value;
	pNew->link=NULL;
	pPre->link=pNew;
	}

/*This function returns a linked list of the digits of 2 to the power power.*/
NODE *powerTwo(int power) {
	int carryOver=0;
	int temp;
	int i;
	int stop;
	NODE *ones;
	NODE *currentDigit;
//	NODE first;
//	NODE tempNode;

	ones=malloc(sizeof(NODE));
	ones->data.digit=1;
	ones->link=NULL;

	for (i=1;i<=power;i++){
		currentDigit=ones;
		stop=0;
		carryOver=0;
//		printf("%d",i);
		while (!(stop))
			{
			temp=currentDigit->data.digit*2+carryOver;
			currentDigit->data.digit=temp%10;
			carryOver=temp/10;
			if (currentDigit->link==NULL)
				{stop=1;}
			else
				{
				currentDigit=currentDigit->link;
				}
			}
	
		if (carryOver>0)
			{
			newPower(currentDigit,carryOver);
			}
		}
		return ones; 
		}
