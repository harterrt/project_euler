import EulerModule

def main():
	output = []

	primes = EulerModule.primeList(10000)
	for prime in EulerModule.primeList(1000):
		primes.remove(prime)
	
	for aa in range (0, len(primes) - 3):
		for bb in range(aa + 1, len(primes) - 2):
			if isPermute(primes[aa], primes[bb]):
				step = primes[bb] - primes[aa]
				if isPermute(primes[aa], primes[bb] + step):
					if EulerModule.searchList(primes, primes[bb] + step) != -1:
						output.append( (primes[aa], primes[bb], primes[bb] + step) )
	
	return output


def isPermute(aa, bb):
	one = list(str(aa))
	one.sort()
	two = list(str(bb))
	two.sort()

	return one == two
