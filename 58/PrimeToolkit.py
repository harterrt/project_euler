from math import floor

class PrimeSieve():
    def __init__(self,ceiling=120):
        self.__ceiling__ = ceiling

        #For convenience sake, include 0 and 1 in the seive (then seive[7] refers to 7 not 9)
        self.sieve = [True]*(self.__ceiling__+1)
        self.sieve[0] = False
        self.sieve[1] = False

        for base in range(2,len(self.sieve)):
            #Multiples of composites have already been blanked by their factors
            if self.sieve[base]:
                #Multipliers below the base have already been run
                for multiplier in range(base,int(floor(self.__ceiling__/base))+1):
                    self.sieve[base*multiplier]=False

