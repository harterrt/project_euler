import unittest
import PrimeToolkit

class TestPrimeSieve(unittest.TestCase):
    def testFirstFive(self):
        self.assertEqual(PrimeToolkit.PrimeSieve(11).sieve, 
         [False,False,True,True,False,True,False,True,False,False,False,True])

    def testFirstFiveAndChange(self):
        self.assertEqual(PrimeToolkit.PrimeSieve(12).sieve, 
         [False,False,True,True,False,True,False,True,False,False,False,True,False])

if __name__ == "__main__":
    unittest.main()
