

def permute_str(input):
    output = set()

    if len(input) == 1:
        output.add(input)
    else:
        for digit in range(len(input)):
            #Permute remaining digits
            tails = permute_str(input[0:digit] + input[digit+1:])
            
            for tail in tails:
                output.add(input[digit] + tail)

    return output
